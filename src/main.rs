use argh::FromArgs;
use std::path::PathBuf;
use std::process::exit;

mod gif_decoder;

#[derive(FromArgs)]
/// Reach new heights.
struct GifArgs {
    /// gif file
    #[argh(option)]
    gif_file: PathBuf,
}

fn main() {
    let up: GifArgs = argh::from_env();
    if !up.gif_file.exists() || !up.gif_file.is_file() {
        eprintln!("Path: {:?} must exist and be a regular file", up.gif_file);
        exit(1);
    }
    gif_decoder::decode(&up.gif_file);
}
