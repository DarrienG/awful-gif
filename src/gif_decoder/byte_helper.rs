/// We have two bytes in little endian and need to reorder and combine them
/// Here we do just that. The internal implementation taking the first byte as is,
/// while smashing the second byte onto the left 8 bits of the first byte.
pub fn byte_stitch(byte1: u8, byte2: u8) -> u16 {
    let byte1u16 = byte1 as u16;
    let mut byte2u16 = byte2 as u16;

    byte2u16 <<= 8;

    let mut result: u16 = 0;
    result |= byte1u16;
    result |= byte2u16;
    result
}
