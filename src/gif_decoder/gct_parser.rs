use super::important_bytes::GLOBAL_COLOR_TABLE_START_LOCATION;

pub struct RGB {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

pub fn decode_gct(bytes: &[u8], end_byte: usize) -> Vec<RGB> {
    let mut gct = Vec::new();
    for i in (GLOBAL_COLOR_TABLE_START_LOCATION..end_byte as usize).step_by(3) {
        let red = bytes[i];
        let green = bytes[i + 1];
        let blue = bytes[i + 2];
        gct.push(RGB { red, green, blue });
    }
    gct
}
