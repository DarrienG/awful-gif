use super::important_bytes::COMMENT_CONTROL_EXTENSION_START;
use std::char;
use std::fmt::Write;

pub struct CommentData {
    pub comment: String,
    pub next_block_start_byte: usize,
}

pub enum Comment {
    DoesNotExist,
    Exists(CommentData),
}

pub fn parse_comment(bytes: &[u8], start_byte: usize) -> Comment {
    if !comment_exists(bytes, start_byte) {
        return Comment::DoesNotExist;
    }

    // Comment exists, time to parse for 255 bytes or until we get 00
    let mut comment = String::new();
    let mut comment_stop = start_byte;
    for (idx, byte) in bytes
        .iter()
        .skip(start_byte + 2)
        .take(start_byte + 255)
        .enumerate()
    {
        comment_stop = idx;
        if *byte == 0x00 {
            return Comment::Exists(CommentData {
                comment,
                next_block_start_byte: comment_stop,
            });
        }

        // this is called awful_gif for a reason, this should never fail, but whatever
        write!(&mut comment, "{}", char::from_u32(*byte as u32).unwrap()).unwrap();
    }

    return Comment::Exists(CommentData {
        comment,
        next_block_start_byte: comment_stop,
    });

    // the next byte after this parsing should be a ',' signifying data start, let's check that next
}

fn comment_exists(bytes: &[u8], start_byte: usize) -> bool {
    if bytes.len() < start_byte + 1 {
        false
    } else {
        bytes[start_byte] == COMMENT_CONTROL_EXTENSION_START[0]
            && bytes[start_byte + 1] == COMMENT_CONTROL_EXTENSION_START[1]
    }
}
