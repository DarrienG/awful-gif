use super::byte_helper::byte_stitch;
use super::important_bytes::GRAPHIC_CONTROL_EXTENSION_START;

#[derive(Debug)]
pub struct ControlExtensionMetadata {
    pub block_size: u8, // it's always 4, why do we have to parse this (see block size in graphic control extension: https://www.w3.org/Graphics/GIF/spec-gif89a.txt)
    pub disposal_method: DisposalMethod, // techically u3
    pub user_input_flag: bool, // i have never seen this in real life
    pub delay_time: u16,
    pub transparent_color_index: Option<u8>,
    pub next_block_start: usize, // a relay to the caller where to go next so we can stop using gct end byte
}

#[derive(Debug)]
pub enum DisposalMethod {
    NoDisposal,
    DoNotDispose, // not sure how these two are different
    RestoreBackground,
    RestorePreviousGraphic,
    Reserved, // perhaps we'll finally use this in gif2022
}

#[derive(Debug)]
pub enum GraphicControlExtension {
    Malformed,
    DoesNotExist,
    Exists(ControlExtensionMetadata),
}

/// optional field
pub fn get_graphic_control_extension(bytes: &[u8], gct_end_byte: usize) -> GraphicControlExtension {
    if !graphic_control_extension_exists(bytes, gct_end_byte) {
        return GraphicControlExtension::DoesNotExist;
    }

    let gce_start_byte = (gct_end_byte + 2) as usize;
    let block_size = 4;
    if bytes[gce_start_byte] != 4 {
        eprintln!("Graphic control extension block size is not 4. Malformed block");
        return GraphicControlExtension::Malformed;
    }

    let packed_gif_data_byte = bytes[gce_start_byte + 1];
    let disposal_method = get_disposal_method(packed_gif_data_byte);
    let user_input_flag = get_user_input_flag(packed_gif_data_byte);
    let transparent_color_flag = get_transparent_color_flag(packed_gif_data_byte);
    let delay_time = get_delay_time(bytes, gce_start_byte);
    let transparent_color_index = if transparent_color_flag {
        Some(get_transparent_color_index(bytes, gce_start_byte))
    } else {
        None
    };
    confirm_block_terminated(bytes, gce_start_byte);

    GraphicControlExtension::Exists(ControlExtensionMetadata {
        block_size,
        disposal_method,
        user_input_flag,
        delay_time,
        transparent_color_index,
        next_block_start: gce_start_byte + 6,
    })
}

fn confirm_block_terminated(bytes: &[u8], gce_start_byte: usize) {
    let terminated_byte = bytes[gce_start_byte + 5];
    if terminated_byte == 0 {
        println!("graphic control extension properly terminated!")
    } else {
        eprintln!("graphic control extension improperly terminated. Requires 0 block to end but got: {}, continuing anyway", terminated_byte);
    }
}

fn get_transparent_color_index(bytes: &[u8], gce_start_byte: usize) -> u8 {
    bytes[gce_start_byte + 4]
}

fn get_delay_time(bytes: &[u8], gce_start_byte: usize) -> u16 {
    let byte1 = bytes[gce_start_byte + 2];
    let byte2 = bytes[gce_start_byte + 3];
    byte_stitch(byte1, byte2)
}

fn get_transparent_color_flag(mut packed_byte: u8) -> bool {
    let mask: u8 = 0b00000001;
    packed_byte &= mask;
    packed_byte != 0
}

/// This is almost certainly not used. But we're already here, might as well parse away.
fn get_user_input_flag(mut packed_byte: u8) -> bool {
    let mask: u8 = 0b00000010;
    packed_byte &= mask;
    packed_byte >>= 1;
    packed_byte != 0
}

fn get_disposal_method(mut packed_byte: u8) -> DisposalMethod {
    let mask: u8 = 0b00011100;
    packed_byte &= mask;
    packed_byte >>= 2;
    match packed_byte {
        0 => DisposalMethod::NoDisposal,
        1 => DisposalMethod::DoNotDispose,
        2 => DisposalMethod::RestoreBackground,
        3 => DisposalMethod::RestorePreviousGraphic,
        _ => DisposalMethod::Reserved, // technically this is a malformed gif if we ever hit this
    }
}

fn graphic_control_extension_exists(bytes: &[u8], gct_end_byte: usize) -> bool {
    bytes[gct_end_byte + 1] == GRAPHIC_CONTROL_EXTENSION_START
}
