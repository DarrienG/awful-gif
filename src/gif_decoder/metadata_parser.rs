use super::byte_helper::byte_stitch;
use std::u16;
use thiserror::Error;

use super::important_bytes::{
    BACKGROUND_START_LOCATION, GIF_89_BYTES, GIF_HEADER_SIZE, GLOBAL_COLOR_TABLE_METADATA_LOCATION,
    METADATA_END_BYTE,
};

#[derive(Debug, Copy, Clone)]
pub struct ColorTableMetadata {
    pub color_resolution: u8, // really it's a u3, but whatever
    pub sorted: bool,
    pub end_byte: usize, // decoded end byte of GCT
}

pub struct Dimensions {
    pub height: u16,
    pub width: u16,
}

#[derive(Error, Debug)]
pub enum DecodeError {
    #[error("Malformed gif")]
    MalformedGif,
}

pub enum GctMetadata {
    Malformed,
    DoesNotExist,
    Exists(ColorTableMetadata),
}

pub fn is_gif(bytes: &[u8]) -> bool {
    for (i, byte) in bytes.iter().enumerate() {
        if i == 5 && *byte == GIF_89_BYTES[i] {
            return true;
        }
        if GIF_89_BYTES[i] != *byte {
            return false;
        }
    }
    return false;
}

pub fn get_dimensions(bytes: &[u8]) -> Result<Dimensions, DecodeError> {
    if bytes.len() < 10 {
        return Err(DecodeError::MalformedGif);
    }
    let width_byte1 = bytes[6];
    let width_byte2 = bytes[7];
    let width = byte_stitch(width_byte1, width_byte2);

    let height_byte1 = bytes[8];
    let height_byte2 = bytes[9];
    let height = byte_stitch(height_byte1, height_byte2);

    Ok(Dimensions { height, width })
}

pub fn get_gct_metadata(bytes: &[u8]) -> GctMetadata {
    if bytes.len() < GLOBAL_COLOR_TABLE_METADATA_LOCATION {
        return GctMetadata::Malformed;
    }

    let gct_byte = bytes[GLOBAL_COLOR_TABLE_METADATA_LOCATION];

    if !gct_exists(gct_byte) {
        return GctMetadata::DoesNotExist;
    }

    let resolution = color_resolution(gct_byte);
    let sorted = is_sorted(gct_byte);
    let size = gct_size(gct_byte);

    GctMetadata::Exists(ColorTableMetadata {
        color_resolution: resolution,
        sorted,
        end_byte: (size + GIF_HEADER_SIZE as u16) as usize,
    })
}

pub fn confirm_metadata_end_magic(bytes: &[u8], end_byte: usize) -> Result<(), DecodeError> {
    let end_byte = end_byte as usize;
    if bytes[end_byte] != METADATA_END_BYTE {
        Err(DecodeError::MalformedGif)
    } else {
        Ok(())
    }
}

pub fn get_background_color(bytes: &[u8]) -> u8 {
    bytes[BACKGROUND_START_LOCATION]
}

fn gct_size(byte: u8) -> u16 {
    // see: https://www.ntfs.com/gif-signature-format.htm
    (1 << ((byte & 0x07) + 1)) * 3
}

fn is_sorted(mut byte: u8) -> bool {
    let mask: u8 = 0b00001000;
    byte &= mask;
    byte >>= 3;
    byte == 1
}

fn color_resolution(mut byte: u8) -> u8 {
    let mask: u8 = 0b01110000;
    byte &= mask;
    byte >>= 4;
    // now we interpret it as a number rather than a byte
    byte
}

fn gct_exists(mut byte: u8) -> bool {
    let mask: u8 = 0b10000000;
    byte &= mask;
    byte >>= 7;
    byte != 0
}
