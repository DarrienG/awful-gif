pub const GIF_HEADER_SIZE: u8 = 13;
pub const GLOBAL_COLOR_TABLE_METADATA_LOCATION: usize = 0xA;
pub const GIF_89_BYTES: [u8; 6] = [0x47, 0x49, 0x46, 0x38, 0x39, 0x61];
pub const BACKGROUND_START_LOCATION: usize = 0xB;
// don't care, but the option is there
#[allow(dead_code)]
pub const PIXEL_ASPECT_RATIO_LOCATION: usize = 0xC;
pub const GLOBAL_COLOR_TABLE_START_LOCATION: usize = 0xD;
pub const METADATA_END_BYTE: u8 = 0x21;
pub const GRAPHIC_CONTROL_EXTENSION_START: u8 = 0xF9;
pub const COMMENT_CONTROL_EXTENSION_START: [u8; 2] = [0x21, 0xFE];
