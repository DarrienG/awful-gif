use comment_parser::{parse_comment, Comment};
use graphic_control_extension_parser::{get_graphic_control_extension, GraphicControlExtension};
use metadata_parser::{
    confirm_metadata_end_magic, get_background_color, get_dimensions, get_gct_metadata, is_gif,
    ColorTableMetadata, GctMetadata,
};
use std::{fs::File, io::Read, path::Path};

pub mod byte_helper;
mod comment_parser;
mod gct_parser;
mod graphic_control_extension_parser;
pub mod important_bytes;
mod metadata_parser;

/// later on this will pass the data it gets up in a struct
/// for now let's just print all the crap out
pub fn decode(path: &Path) {
    let mut data = Vec::new();
    let mut f = File::open(path).expect("File suddenly does not exist");
    let _ = f.read_to_end(&mut data);

    if is_gif(&data) {
        println!("Hey look at that, it's a gif!");
    } else {
        println!("Either magic not set properly, or this isn't a gif");
    }

    match get_dimensions(&data) {
        Ok(dimensions) => println!("Gif dimensions: {}x{}", dimensions.width, dimensions.height),
        Err(e) => eprintln!("{}", e),
    }

    match get_gct_metadata(&data) {
        GctMetadata::Malformed => {
            eprintln!("Malformed gif! This is an error in your code right now, fix it!!!")
        }
        GctMetadata::DoesNotExist => {
            eprintln!("GCT does not exist! This is an error in your code right now, fix it!!!")
        }
        GctMetadata::Exists(color_table_metadata) => {
            parse_gif_with_gct(&data, color_table_metadata)
        }
    }
}

fn parse_gif_with_gct(data: &[u8], color_table_metadata: ColorTableMetadata) {
    println!("Color table metadata: {:?}", color_table_metadata);
    let background_color = get_background_color(&data);
    println!("background color: {}", background_color);

    let gct = gct_parser::decode_gct(&data, color_table_metadata.end_byte);
    println!("Got GCT with colors:");
    for (idx, row) in gct.iter().enumerate() {
        println!(
            "\tidx: {}, red: {}, green: {}, blue: {}",
            idx, row.red, row.green, row.blue
        );
    }

    match confirm_metadata_end_magic(data, color_table_metadata.end_byte) {
        Ok(()) => println!("gif metadata ends with magic properly!"),
        Err(e) => eprintln!("gif metadata end improperly, magic not set right! {}", e),
    }

    match get_graphic_control_extension(&data, color_table_metadata.end_byte) {
        GraphicControlExtension::Malformed => eprintln!("Improperly formed GCE"),
        GraphicControlExtension::DoesNotExist => println!("GCE does not exist!"),
        GraphicControlExtension::Exists(metadata) => {
            println!(
                "Graphic control extension exists! Data in extension: {:?}",
                metadata
            );
            match parse_comment(data, metadata.next_block_start) {
                Comment::DoesNotExist => println!("No comment! ;)"),
                Comment::Exists(comment_data) => {
                    println!("Comment found: `{}`", comment_data.comment)
                }
            }
        }
    }
}
