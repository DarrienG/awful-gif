# It's bad

I used this for learning. It just parses some gifs but doesn't render them. If
you really want to try it on a gif you can. Install rust and run:

```
cargo run --release --quiet -- --gif-file ./experiments/Sunflower_as_gif_websafe.gif
```

It hasn't been fuzz tested and definitely doesn't work on all gifs. It was a
good opportunity for learning though!

If you'd like to learn more about my journey while writing this, check out my
blogpost on gifs at:

https://blog.darrien.dev/you-dont-know-gif
